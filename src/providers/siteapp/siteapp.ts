//import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { LoadingController } from 'ionic-angular';

/*
  Generated class for the SiteappProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SiteappProvider {

  public url_padrao = "http://testewp-com-br.umbler.net";
  public loader;

  constructor(public http: Http, public loadingCtrl: LoadingController) {
    console.log('Hello SiteappProvider Provider');
  }

  solicitaGetApi(url_metodo) {
    return this.http.get(this.url_padrao + url_metodo);
  }

  abreLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando..."
    });
    this.loader.present();
  }

  fechaLoading() {
    this.loader.dismiss();
  }

}
