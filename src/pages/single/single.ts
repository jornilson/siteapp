import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SiteappProvider } from '../../providers/siteapp/siteapp';

/**
 * Generated class for the SinglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-single',
  templateUrl: 'single.html',
    providers: [
      SiteappProvider
    ]
})
export class SinglePage {

  public noticia_id;
  public titulo;
  public conteudo;
  public imagem;

  constructor(public navCtrl: NavController, public navParams: NavParams, public siteappProvider: SiteappProvider) {
  }

  carregaConteudo() {
    this.noticia_id = this.navParams.get('id');
    this.siteappProvider.abreLoading();

    const url_metodo = "/wp-json/wp/v2/posts/" + this.noticia_id;
    this.siteappProvider.solicitaGetApi(url_metodo).subscribe(
      data => {
        const resposta = (data as any);
        const obj_retorno = JSON.parse(resposta._body);
        
        this.titulo = obj_retorno.title.rendered;
        this.conteudo = obj_retorno.content.rendered
        
        //console.log(obj_retorno.content.rendered);

        console.log(obj_retorno);
  
        this.siteappProvider.fechaLoading(); 
        
      },
      error => {
        console.log(error);
        this.siteappProvider.fechaLoading();
      }
    );

  }

  ionViewDidEnter() {    
    this.carregaConteudo();
  }

}
