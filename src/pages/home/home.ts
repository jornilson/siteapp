import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SiteappProvider } from '../../providers/siteapp/siteapp';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { SinglePage } from '../single/single';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [
    SiteappProvider
  ]
})
export class HomePage {

  public lista_noticia = new Array<any>();
  public refresher;
  public carregando: boolean = false;
 
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public siteappProvider: SiteappProvider    
  ) {}

  carregalista(){   
    this.siteappProvider.abreLoading();

    const url_metodo = "/wp-json/wp/v2/posts?context=view";
    this.siteappProvider.solicitaGetApi(url_metodo).subscribe(
      data => {
        const resposta = (data as any);
        const obj_retorno = JSON.parse(resposta._body);
       // console.log(obj_retorno);
        this.lista_noticia = obj_retorno;
        this.siteappProvider.fechaLoading();
        //se o componente refresher estiver visivel, chama o metodo de concluir ele e seta o valor false na variavel carregando.
        if (this.carregando) {
          this.refresher.complete();
          this.carregando = false;
        }
      },
      error => {
        console.log(error);
        this.siteappProvider.fechaLoading();
      }
    );

  }

  doRefresh(refresher) {
    this.refresher = refresher;
    this.carregando = true;
    this.carregalista();
  }

  ionViewDidEnter() { 
    
    this.carregalista();
  }

  abrirDetalhes(noticia){  
    this.navCtrl.push(SinglePage, {id: noticia.id});
  }

}
